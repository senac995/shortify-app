const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema
 const UrlSchema = new Schema({

    url: {
        type: String,
        required: true
    },
    shortcode: {
        type: String,
        required: true
    }
})
module.exports = Url = mongoose.model('urls', UrlSchema);