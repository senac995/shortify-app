const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Url = require('../../models/Urls');

// @route   GET /api/urls/test
// @desc    Test posts route
//@access   Public
router.get('/test', (req,res) => res.json({msg: "posts works"}));

// @route   GET /api/urls
// @desc    Get all the urls
//@access   Public
router.get('/', (req, res) => {
    Url.find()
    .then(urls => res.json(urls))
    .catch(err => res.status(404));
});

// @route   POST /api/urls
// @desc    Post an Url with its shorten code
//@access   Public
router.post('/', (req,res) => {

    const newUrl = new Url({
        url: req.body.url,
        shortcode: req.body.shortcode
    });

    newUrl.save()
    .then(url => res.json(url));
});

// @route   GET /api/urls/:shortcode
// @desc    Get url by shortcode
//@access   Public
router.get('/:shortcode', (req, res) => {
    Url.findOne({ shortcode: req.params.shortcode })
    .then(url => res.json(url))
    .catch(err => res.status(404).json({ noUrlFound: 'no url found with that shortcode' }))
});

module.exports = router;