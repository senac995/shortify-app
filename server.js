const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const urls = require('./routes/api/urls');

// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// Configuring the database
const dbConfig = require('./config/database.config.js').url;

mongoose
.connect(dbConfig)
.then(() => console.log('mongodb connected'))
.catch(err => console.log(err));

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to nodejs url shorterner application"});
});

//use url routes
app.use('/api/urls', urls);

// listen for requests
app.listen(process.env.PORT || 5000, () => {
    console.log("Server is listening on port 5000");
});